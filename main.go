package main

import (
	"fmt"
	"net/http"

	authenticationController "github.com/Deaware-RD/KTB-ACS-IoT-Box/src/controllers/authentication"
	employeeController "github.com/Deaware-RD/KTB-ACS-IoT-Box/src/controllers/employee"
	datasource "github.com/Deaware-RD/KTB-ACS-IoT-Box/src/datasources"
	model "github.com/Deaware-RD/KTB-ACS-IoT-Box/src/models"
	service "github.com/Deaware-RD/KTB-ACS-IoT-Box/src/services"
	backendmqtt "github.com/Deaware-RD/KTB-ACS-IoT-Box/src/services/backendmqtt"
	"github.com/Deaware-RD/KTB-ACS-IoT-Box/src/services/config"
	"github.com/Deaware-RD/KTB-ACS-IoT-Box/src/services/device"
	"github.com/Deaware-RD/KTB-ACS-IoT-Box/src/services/deviceconfig"
	localauthenticator "github.com/Deaware-RD/KTB-ACS-IoT-Box/src/services/localAuthenticator"
	localmqtt "github.com/Deaware-RD/KTB-ACS-IoT-Box/src/services/localmqtt"

	"github.com/labstack/echo/v4"
)

func main() {
	if err := config.Init(); err != nil {
		panic(err)
	}

	if err := device.Init(); err != nil {
		panic(err)
	}

	fmt.Printf("Local MQTT URL : %s\r\n", config.Get().LocalMqttUrl)
	var localMqttOptions service.MqttClientOptions = service.MqttClientOptions{
		Url: config.Get().LocalMqttUrl,
	}
	if err := localmqtt.Init(&localMqttOptions); err != nil {
		panic(err)
	}

	fmt.Printf("Back-end MQTT URL : %s\r\n", config.Get().BackendMqttUrl)
	var backendMqttOptions service.MqttClientOptions = service.MqttClientOptions{
		Url: config.Get().BackendMqttUrl,
	}
	if err := backendmqtt.Init(&backendMqttOptions); err != nil {
		panic(err)
	}

	db, err := datasource.Init()
	if err != nil {
		panic(err)
	}

	if err := deviceconfig.Init(db); err != nil {
		panic(err)
	}

	localauthenticator.Init(db)

	db.AutoMigrate(&model.EmployeeLocal{})

	e := echo.New()
	e.GET("/", func(c echo.Context) error {
		return c.String(http.StatusOK, "Hello, World!")
	})

	e.POST("/authenticate", authenticationController.NewAuthenticationController(db))

	e.GET("employee/:employeeId/picture", employeeController.NewEmployeePictureController(db))

	fmt.Print("System started.\r\n")

	// e.Logger.Debug(e.StartTLS(":443", "cert/cert.crt", "cert/key.key"))
	e.Logger.Debug(e.Start(":8080"))
}
