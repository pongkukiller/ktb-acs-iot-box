source ./env-application
source ./env-build-target

echo "Output directory : $OUTPUT_DIR"
echo "Package name : $PACKAGE_NAME"

mkdir -p ./$OUTPUT_DIR/
go build -ldflags "-linkmode external -extldflags '-static' -s -w" -v -a -o ./$OUTPUT_DIR/$PACKAGE_NAME main.go