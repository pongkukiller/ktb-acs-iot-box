CREATE TABLE `employee_local` (
	`employee_local_id` INTEGER AUTO_INCREMENT,
	`employee_id` INTEGER UNIQUE,
	`name` TEXT,
	`position` TEXT,
	`employee_code` TEXT UNIQUE,
	`mifare_id` TEXT UNIQUE,
	`begin_time` TEXT,
	`end_time` TEXT
);