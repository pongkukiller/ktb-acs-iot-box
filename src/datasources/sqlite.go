package datasource

import (
	model "github.com/Deaware-RD/KTB-ACS-IoT-Box/src/models"
	"gorm.io/driver/sqlite"
	"gorm.io/gorm"
)

func Init() (*gorm.DB, error) {
	var db *gorm.DB
	var err error

	db, err = gorm.Open(sqlite.Open("gorm.db"), &gorm.Config{})

	db.AutoMigrate(&model.DeviceConfig{})

	return db, err
}
