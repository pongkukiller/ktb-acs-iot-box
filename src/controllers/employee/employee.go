package employee

import (
	"fmt"
	"io/ioutil"
	"net/http"

	"github.com/labstack/echo/v4"
	"gorm.io/gorm"
)

func NewEmployeePictureController(db *gorm.DB) echo.HandlerFunc {
	return func(context echo.Context) error {
		icon, err := ioutil.ReadFile("resources/user_icon.jpg")
		if err != nil {
			return context.JSON(http.StatusInternalServerError, err)
		}

		employeeIdString := context.Param("employeeId")
		fmt.Printf("Employee picture API called : %s\r\n", employeeIdString)

		return context.Blob(http.StatusOK, "image/jpeg", icon)
	}
}
