package authentication

import (
	"net/http"
	"time"

	model "github.com/Deaware-RD/KTB-ACS-IoT-Box/src/models"
	backendmqtt "github.com/Deaware-RD/KTB-ACS-IoT-Box/src/services/backendmqtt"
	device "github.com/Deaware-RD/KTB-ACS-IoT-Box/src/services/device"
	deviceconfig "github.com/Deaware-RD/KTB-ACS-IoT-Box/src/services/deviceconfig"
	localAuthenticator "github.com/Deaware-RD/KTB-ACS-IoT-Box/src/services/localAuthenticator"
	"github.com/labstack/echo/v4"
	"gorm.io/gorm"
)

func NewAuthenticationController(db *gorm.DB) echo.HandlerFunc {
	return func(context echo.Context) error {
		var body model.AuthenticationRequest

		if err := context.Bind(&body); err != nil {
			return err
		}

		// var deviceConfig *model.DeviceConfig
		var err error

		// deviceConfig, err = deviceconfig.Get()

		deviceConfig, err := deviceconfig.Get()
		if err != nil {
			return err
		}

		if body.DeviceSerial == deviceConfig.TabletEnterSerialNumber {
			body.Direction = "enter"
		} else if body.DeviceSerial == deviceConfig.TabletExitSerialNumber {
			body.Direction = "exit"
		}

		var response *model.AuthenticationResponse

		if backendmqtt.IsConnected() {
			response, err = backendmqtt.Authenticate(&body)
		} else {
			response, err = localAuthenticator.Authenticate(&body)
		}

		if err != nil {
			response := model.AuthenticationResponse{
				Result:  "fail",
				Message: err.Error(),
				Time:    time.Now(),
			}
			return context.JSON(http.StatusUnprocessableEntity, response)
		}

		response.Time = time.Now()

		var httpCode int = http.StatusOK

		if response.Result == "success" {
			httpCode = http.StatusOK
			device.OpenDoor()
		} else {
			if len(response.Message) <= 0 {
				response.Message = "ไม่สามารถยืนยันตัวตนได้"
			}
			httpCode = http.StatusUnprocessableEntity
		}

		return context.JSON(httpCode, *response)
	}
}
