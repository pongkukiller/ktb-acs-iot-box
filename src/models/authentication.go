package model

import "time"

type Authentication struct {
	Type string `json:"type"`
	Data string `json:"data"`
}

type AuthenticationRequest struct {
	DeviceSerial    string           `json:"device_serial"`
	Direction       string           `json:"direction"`
	Authentications []Authentication `json:"authentications"`
}

type AuthenticationResponse struct {
	Result   string    `json:"result"`
	Message  string    `json:"message"`
	Employee *Employee `json:"employee"`
	Time     time.Time `json:"time"`
}
