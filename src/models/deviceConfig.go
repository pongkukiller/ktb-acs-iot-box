package model

type DeviceConfig struct {
	DeviceConfigId          int    `gorm:"column:device_config_id;primaryKey;autoIncrement" json:"device_config_id"`
	DoorSensorEnabled       bool   `gorm:"column:door_sensor_enabled" json:"door_sensor_enabled"`
	BreakGlassEnabled       bool   `gorm:"column:break_glass_enabled" json:"break_glass_enabled"`
	BuzzerEnabled           bool   `gorm:"column:buzzer_enabled" json:"buzzer_enabled"`
	BuzzerDelay             int32  `gorm:"column:buzzer_delay" json:"buzzer_delay"`
	TwoFactorEnabled        bool   `gorm:"column:two_factor_enabled" json:"two_factor_enabled"`
	TabletEnterSerialNumber string `gorm:"column:tablet_enter_serial_number" json:"tablet_enter_serial_number"`
	TabletExitSerialNumber  string `gorm:"column:tablet_exit_serial_number" json:"tablet_exit_serial_number"`
}

func (DeviceConfig) TableName() string { return "device_config" }
