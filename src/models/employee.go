package model

type Employee struct {
	EmployeeId   int    `json:"employee_id"`
	Name         string `json:"name"`
	Lastname     string `json:"lastname"`
	Position     string `json:"position"`
	MifareId     string `json:"mifare_id"`
	PictureUrl   string `json:"-"`
	EmployeeCode string `json:"employee_code"`
	Email        string `json:"email"`
	FaceId       string `json:"-"`
	IsExited     bool   `json:"-"`
	Deleted      bool   `json:"-"`
}
