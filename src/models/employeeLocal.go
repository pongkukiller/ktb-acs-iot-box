package model

import "time"

type EmployeeLocal struct {
	EmployeeLocalId int       `gorm:"column:employee_local_id;primaryKey;autoIncrement" json:"employee_local_id"`
	EmployeeId      int       `gorm:"column:employee_id;unique" json:"employee_id"`
	Name            string    `gorm:"column:name" json:"name"`
	Position        string    `gorm:"column:position" json:"position"`
	MifareId        string    `gorm:"column:mifare_id;unique" json:"mifare_id"`
	EmployeeCode    string    `gorm:"column:employee_code;unique" json:"employee_code"`
	BeginTime       time.Time `gorm:"column:begin_time"`
	EndTime         time.Time `gorm:"column:end_time"`
}

func (EmployeeLocal) TableName() string { return "employee_local" }
