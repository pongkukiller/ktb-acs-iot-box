package model

type Config struct {
	BackendBaseUrl string `json:"backend_base_url"`
	BackendMqttUrl string `json:"backend_mqtt_url"`
	LocalMqttUrl   string `json:"local_mqtt_url"`
	SerialNumber   string `json:"serial_number"`
	ModbusPort     string `json:"modbus_port"`
	ModbusBaudrate int    `json:"modbus_baudrate"`
}
