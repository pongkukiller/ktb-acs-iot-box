package model

import "time"

type DeviceStatus struct {
	DeviceStatusId    int32     `gorm:"primary_key;column:device_status_id" json:"device_status_id"`
	DeviceId          int32     `gorm:"column:device_id" json:"device_id"`
	IotBoxStatus      string    `gorm:"column:iot_box_status" json:"iot_box_status"`
	TabletEnterStatus string    `gorm:"column:tablet_enter_status" json:"tablet_enter_status"`
	TabletExitStatus  string    `gorm:"column:tablet_exit_status" json:"tablet_exit_status"`
	DoorStatus        string    `gorm:"column:door_status" json:"door_status"`
	BreakGlass        bool      `gorm:"column:break_glass" json:"break_glass"`
	Buzzer            string    `gorm:"column:buzzer" json:"buzzer"`
	UpdateAt          time.Time `json:"update_at"`
}
