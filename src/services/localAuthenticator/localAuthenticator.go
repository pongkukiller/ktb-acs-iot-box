package localauthenticator

import (
	"fmt"
	"time"

	model "github.com/Deaware-RD/KTB-ACS-IoT-Box/src/models"
	"gorm.io/gorm"
)

var instanceDb *gorm.DB

func Init(db *gorm.DB) error {
	instanceDb = db
	return nil
}

func Authenticate(request *model.AuthenticationRequest) (*model.AuthenticationResponse, error) {
	var firstAuthentication *model.Authentication
	var response model.AuthenticationResponse
	var failResponse = model.AuthenticationResponse{
		Result: "fail",
	}

	if instanceDb == nil {
		return &failResponse, fmt.Errorf("nil instanceDb")
	}

	if len(request.Authentications) <= 0 {
		return &failResponse, fmt.Errorf("invalid authentication method")
	}

	for _, authentication := range request.Authentications {
		if authentication.Type == "mifare" {
			firstAuthentication = &authentication
			break
		}
	}

	if firstAuthentication == nil {
		return nil, fmt.Errorf("no mifare authentication found")
	}

	var employeeLocal model.EmployeeLocal = model.EmployeeLocal{
		MifareId: firstAuthentication.Data,
	}

	if err := instanceDb.Where(&employeeLocal).First(&employeeLocal).Error; err != nil {
		if err == gorm.ErrRecordNotFound {
			return &model.AuthenticationResponse{
				Result:  "fail",
				Message: "Authentication failed.",
			}, nil
		} else {
			return &failResponse, err
		}
	}

	employee := model.Employee{
		EmployeeId:   employeeLocal.EmployeeId,
		Name:         employeeLocal.Name,
		EmployeeCode: employeeLocal.EmployeeCode,
		Position:     employeeLocal.Position,
	}

	response.Result = "success"
	response.Employee = &employee
	response.Time = time.Now()

	return &response, nil
}
