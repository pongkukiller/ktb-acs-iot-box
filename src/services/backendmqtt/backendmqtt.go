package backendmqtt

import (
	"encoding/json"
	"fmt"
	"time"

	model "github.com/Deaware-RD/KTB-ACS-IoT-Box/src/models"
	services "github.com/Deaware-RD/KTB-ACS-IoT-Box/src/services"
	"github.com/Deaware-RD/KTB-ACS-IoT-Box/src/services/config"
	deviceconfig "github.com/Deaware-RD/KTB-ACS-IoT-Box/src/services/deviceconfig"
	mqtt "github.com/eclipse/paho.mqtt.golang"
	"github.com/google/uuid"
)

// ktb/acs/device/<serial_number>/door_status
// ktb/acs/device/<serial_number>/glass_break

// ktb/acs/device/<serial_number>/authenticate/request
// ktb/acs/device/<serial_number>/authenticate/response

type OnConfigReceived func(*model.DeviceConfig)
type OnAuthenticationResponsed func(*model.AuthenticationResponse)

var client mqtt.Client
var clientConnected bool = false

const TOPIC_DEVICE_PREFIX string = "ktb/acs/device"

var responseChannel chan *model.AuthenticationResponse

func onAuthenticateResponsed(client mqtt.Client, msg mqtt.Message) {
	var response model.AuthenticationResponse

	fmt.Printf("Got authenticate response : \r\n")
	fmt.Printf("%v\r\n", string(msg.Payload()))

	if err := json.Unmarshal(msg.Payload(), &response); err != nil {
		fmt.Printf("Failed to parse payload : %s\r\n", err.Error())
		return
	}

	fmt.Printf("Sending response to channel\r\n")
	responseChannel <- &response
}

func onConfigReceived(client mqtt.Client, msg mqtt.Message) {
	var deviceConfig model.DeviceConfig

	fmt.Printf("Device config received\r\n")

	if err := json.Unmarshal(msg.Payload(), &deviceConfig); err != nil {
		fmt.Print(err.Error())
		return
	}

	fmt.Printf("%v\r\n", deviceConfig)

	if err := deviceconfig.Set(&deviceConfig); err != nil {
		return
	}
}

func onForceOpenReceived(client mqtt.Client, msg mqtt.Message) {

}

func onConnected(client mqtt.Client) {
	clientConnected = true
	fmt.Printf("Backend MQTT connected.\r\n")

	topicAuthenticateResponse := fmt.Sprintf("%s/%s/authenticate/response", TOPIC_DEVICE_PREFIX, config.Get().SerialNumber)
	topicConfig := fmt.Sprintf("%s/%s/config", TOPIC_DEVICE_PREFIX, config.Get().SerialNumber)
	topicForceOpen := fmt.Sprintf("%s/%s/force_open", TOPIC_DEVICE_PREFIX, config.Get().SerialNumber)

	client.Subscribe(topicAuthenticateResponse, 2, onAuthenticateResponsed)
	client.Subscribe(topicConfig, 1, onConfigReceived)
	client.Subscribe(topicForceOpen, 1, onForceOpenReceived)
}

func onConnectionLost(client mqtt.Client, err error) {
	clientConnected = false
	fmt.Printf("Backend MQTT connection lost.\r\n")
}

func onReconnecting(client mqtt.Client, options *mqtt.ClientOptions) {
	fmt.Printf("Backend MQTT re-connecting.\r\n")
}

func Authenticate(request *model.AuthenticationRequest) (*model.AuthenticationResponse, error) {
	fmt.Printf("Backend authenticate requesed.\r\n")
	topicAuthenticateRequest := fmt.Sprintf("%s/%s/authenticate/request", TOPIC_DEVICE_PREFIX, config.Get().SerialNumber)
	var response *model.AuthenticationResponse

	if client == nil {
		return nil, fmt.Errorf("no client initiated")
	}

	request.DeviceSerial = config.Get().SerialNumber

	requestJson, err := json.Marshal(*request)
	if err != nil {
		return nil, err
	}

	/* Clear response channel. */
	for len(responseChannel) > 0 {
		<-responseChannel
	}

	for _, authentication := range request.Authentications {
		if authentication.Type == "mifare" {
			fmt.Printf("MIFARE ID : %s\r\n", authentication.Data)
		}
	}

	token := client.Publish(topicAuthenticateRequest, 2, false, requestJson)
	if token.WaitTimeout(10*time.Second) == false {
		fmt.Printf("Backend MQTT authenticate publish timeout.\r\n")
		return nil, fmt.Errorf("mqtt publish failed")
	}

	fmt.Printf("Backend MQTT authenticate published : %s\r\n", topicAuthenticateRequest)

	fmt.Printf("Backend MQTT waiting for response.\r\n")
	select {
	case response = <-responseChannel:
		fmt.Printf("Backend MQTT got response.\r\n")
		return response, nil
	case <-time.After(10 * time.Second):
		fmt.Printf("Backend MQTT waiting for response timeout.\r\n")
		return nil, fmt.Errorf("receive authenticate response timeout")
	}
}

func IsConnected() bool {
	return clientConnected
}

func Init(options *services.MqttClientOptions) error {
	var clientId string = options.ClientId

	responseChannel = make(chan *model.AuthenticationResponse, 1)

	// random clientID
	if len(clientId) == 0 {
		clientId = uuid.New().String()
	}

	opts := mqtt.NewClientOptions()
	opts.AddBroker(options.Url)
	opts.SetAutoReconnect(true)
	opts.SetConnectRetry(true)
	opts.SetConnectRetryInterval(time.Second * 5)
	opts.SetClientID(clientId)
	opts.SetUsername(options.Username)
	opts.SetPassword(options.Password)
	opts.OnConnect = onConnected
	opts.OnConnectionLost = onConnectionLost
	opts.OnReconnecting = onReconnecting
	client = mqtt.NewClient(opts)
	if token := client.Connect(); !token.WaitTimeout(10*time.Second) || (token.Error() != nil) {
		fmt.Println("MQTT CONNECT ERROR :", token.Error())
		return token.Error()
	}

	return nil
}
