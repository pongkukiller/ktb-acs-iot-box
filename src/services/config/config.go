package config

import (
	"encoding/json"
	"flag"
	"io/ioutil"

	model "github.com/Deaware-RD/KTB-ACS-IoT-Box/src/models"
)

var config model.Config

func Get() *model.Config {
	return &config
}

func Init() error {
	var configFileName string = ""

	flag.StringVar(&configFileName, "c", "", "specific configuration file.")
	flag.Parse()

	configData, err := ioutil.ReadFile(configFileName)
	if err != nil {
		return err
	}

	if err := json.Unmarshal(configData, &config); err != nil {
		return err
	}

	return nil
}
