package services

type MqttClientOptions struct {
	ClientId string
	Url      string
	Username string
	Password string
}
