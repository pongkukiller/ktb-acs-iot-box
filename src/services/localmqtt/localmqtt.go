package localmqtt

import (
	"fmt"
	"time"

	model "github.com/Deaware-RD/KTB-ACS-IoT-Box/src/models"
	services "github.com/Deaware-RD/KTB-ACS-IoT-Box/src/services"
	mqtt "github.com/eclipse/paho.mqtt.golang"
	"github.com/google/uuid"
)

const prefixTopic string = "ktb/acs"

var client mqtt.Client

func onConnected(client mqtt.Client) {
	fmt.Print("Local MQTT connected.\r\n")
}

func onConnectionLost(client mqtt.Client, err error) {
	fmt.Print("Local MQTT connection lost.\r\n")
}

func onReconnecting(client mqtt.Client, options *mqtt.ClientOptions) {
	fmt.Print("Local MQTT re-connecting.\r\n")
}

func PublishConfig(config *model.DeviceConfig) error {
	token := client.Publish(fmt.Sprintf("%s/devices/config", prefixTopic), 1, true, *config)
	if !token.Wait() {
		return fmt.Errorf("failed to publish config")
	}

	return nil
}

func Init(options *services.MqttClientOptions) error {
	var clientId string = options.ClientId

	// random clientID
	if len(clientId) == 0 {
		clientId = uuid.New().String()
	}

	opts := mqtt.NewClientOptions()
	opts.AddBroker(options.Url)
	opts.SetAutoReconnect(true)
	opts.SetConnectRetry(true)
	opts.SetConnectRetryInterval(time.Second * 5)
	opts.SetClientID(clientId)
	opts.SetUsername(options.Username)
	opts.SetPassword(options.Password)
	opts.OnConnect = onConnected
	opts.OnConnectionLost = onConnectionLost
	opts.OnReconnecting = onReconnecting
	client = mqtt.NewClient(opts)
	if token := client.Connect(); !token.WaitTimeout(10*time.Second) || (token.Error() != nil) {
		fmt.Println("MQTT CONNECT ERROR :", token.Error())
		return token.Error()
	}

	return nil
}
