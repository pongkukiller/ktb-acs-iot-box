module github.com/Deaware-RD/KTB-ACS-IoT-Box

go 1.15

require (
	github.com/eclipse/paho.mqtt.golang v1.3.5
	github.com/go-resty/resty/v2 v2.6.0 // indirect
	github.com/goburrow/modbus v0.1.0 // indirect
	github.com/goburrow/serial v0.1.0 // indirect
	github.com/google/uuid v1.2.0
	github.com/karalabe/xgo v0.0.0-20191115072854-c5ccff8648a7 // indirect
	github.com/kelseyhightower/envconfig v1.4.0 // indirect
	github.com/labstack/echo/v4 v4.3.0
	github.com/mattn/go-sqlite3 v1.14.7 // indirect
	github.com/mochi-co/mqtt v1.0.0
	github.com/sarulabs/di v2.0.0+incompatible // indirect
	gorm.io/driver/sqlite v1.1.4
	gorm.io/gorm v1.21.10
)
